
/*
If the user clicks on an element which has the class "ua-choice":
* fetch the element's textContent: for example, "IE 11"
* pass it into the background page's setUaString() function
*/
document.addEventListener("click", (e) => {
    if (e.target.id === "popup") {
        let createData = {
            type: "popup",
        };
        let creating = browser.windows.create(createData);
        creating.then(() => {
            console.log("The popup has been created");
        });
    }
});

