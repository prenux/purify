"use-strict";

var blacklist;
var whitelist;

function onError(e) {
  console.error(e);
}

function checkStoredSettings(storedSettings) {
  if (!storedSettings.blacklist) {
    console.log("Initializing blacklist");
    browser.storage.local.set({blacklist});
  }
}

blacklist = browser.storage.local.get("blacklist");
whitelist = browser.storage.local.get("whitelist");

const gettingStoredSettings = browser.storage.local.get();
gettingStoredSettings.then(checkStoredSettings, onError);

if(blacklist == null){
    blacklist = [];
}

blacklist.push("google");

if (whitelist == null){
    console.log("whitlist is empty");
    whitelist = [];
}

var notif = "purify-notif";

//var buttons = [
//  {
//    "title": "Blacklist"
//  }, {
//    "title": "Whitelist"
//  }
//];

browser.browserAction.onClicked.addListener(()=> {
    var clearing = browser.notifications.clear(notif);
    clearing.then(() => {
        console.log("Cleared");
    });
});

//browser.notifications.onButtonClicked.addListener((id, index) => {
//  browser.notifications.clear(id);
//  console.log("You chose: " + buttons[index].title);
//});

function notify(message) {
  browser.notifications.create(notif, {
    "type": "basic",
    "title": "Purify",
    "message": message, //"buttons": buttons not yet supported
  });
}

function cancel(e) {
    for (var header of e.requestHeaders) {
        if (header.name.toLowerCase() === "host") {
            notify("Requested: " + e.url);
            for(var host of blacklist){
                if(header.value.toLowerCase().includes(host)){
                    console.log("Cancelling: " + header.value);
                    notify("Cancelled: " + e.url);
                    return {cancel: true};
                }
            }
        }
    }
}

browser.webRequest.onBeforeSendHeaders.addListener(
  cancel,
  {urls:["<all_urls>"]},
  ["blocking", "requestHeaders"]
);
